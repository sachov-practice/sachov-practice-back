import { writeFileSync } from "fs";
import path from "path";
import { Column, DataType, Model, Sequelize, Table } from 'sequelize-typescript';

// Part 1

const filePathForTask1 = path.resolve(__dirname, 'output', 'output1.json');

// const [ ,,CHAR ] = process.argv;
const { EMAIL_NAME, EMAIL_DOMAIN, EMAIL_LOCAL } = process.env;

const task1 = () => {
  if (!EMAIL_NAME) {
    writeFileSync(filePathForTask1, JSON.stringify({ message: 'Агрумент EMAIL_NAME не был передан' }));
    return;
  }

  if (!EMAIL_DOMAIN) {
    writeFileSync(filePathForTask1, JSON.stringify({ message: 'Агрумент EMAIL_DOMAIN не был передан' }));
    return;
  }

  if (!EMAIL_LOCAL) {
    writeFileSync(filePathForTask1, JSON.stringify({ message: 'Агрумент EMAIL_LOCAL не был передан' }));
    return;
  }

  const result = `${EMAIL_NAME}@${EMAIL_DOMAIN}.${EMAIL_LOCAL}`;

  writeFileSync(filePathForTask1, JSON.stringify({
    input: {
      EMAIL_NAME,
      EMAIL_DOMAIN,
      EMAIL_LOCAL
    },
    result,
  }));
}

task1();

// Part 2
const filePathForTask2 = path.resolve(__dirname, 'output', 'output2.json');

const initDB = async () => {
  @Table({})
  class Student extends Model {
    @Column({
      type: DataType.UUID,
      defaultValue: DataType.UUIDV4,
      primaryKey: true,
    })
    declare id: string;

    @Column({
      type: DataType.STRING,
      allowNull: false,
    })
    surname: string;

    @Column({
      type: DataType.STRING,
      allowNull: false,
    })
    name: string;

    @Column({
      type: DataType.DATEONLY,
      allowNull: false,
    })
    birthdayDate: Date;
  }

  const sequelize = new Sequelize({
    dialect: 'postgres',
    host: 'database',
    port: 5432,
    username: 'postgres',
    password: 'qwerty',
    database: 'sachkov_practice_db',
  })

  sequelize.addModels([Student]);
  
  try {
    await sequelize.authenticate();
    console.log('Init sequelize OK');
  } catch (error) {
    console.error('Init sequelize ERROR', error);
  }

  const studenList = await Student.findAll({
    order: [['birthdayDate', 'ASC']]
  })

  writeFileSync(filePathForTask2, JSON.stringify(
    studenList
  ));
}

initDB();
